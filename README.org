* Software as a Craft dev blog

This is my swaac (Software as a Craft) web site posts, converted into [[https://orgmode.org/][Emacs Org Mode]] which is another sort of mark-down type writing experience, only richer than straight Markdown. Org mode includes a richer environment for editing code snippets, which is part-and-parcel of *this* particular web site. Org mode is also a great way to organize thoughts and concepts. The major inhibiting factor has been how to publish the results. I'm just stuffing these onto github, which handles org formatting at least as well as it does Markdown. This feels like the least restrictive way for me to write my devblog.

** License

   This blog is licensed Creative Commons Attribution-NonCommercial-ShareAlike (CC BY-NC-SA). This license lets others remix, tweak, and build upon your work non-commercially, as long as they credit you and license their new creations under the identical terms. [[https://creativecommons.org/licenses/by-nc-sa/4.0][See License Deed]]. [[https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode][View legal code]].

[[https://licensebuttons.net/l/by-nc-sa/3.0/88x31.png]]
