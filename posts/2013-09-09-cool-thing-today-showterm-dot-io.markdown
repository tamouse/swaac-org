# LINK: Cool Thing Today: showterm.io

- last update: Time-stamp: <2018-10-12 00:57:04 tamara.temple>
- create date: 2013-09-09 15:10
- keywords: coding, demos, show-and-tell, tools

[http://showterm.io](http://showterm.io "Showterm.io") is a really
cool website for sharing terminal sessions by capturing them and
playing them back.

Easy to install and use, you can share your efforts from the command
line in to others, embed in posts, and so on.

I like this tool!

[Showterm of me making a new post](http://showterm.io/03fe34182d9d48fa45e09#fast )

<iframe src="http://showterm.io/03fe34182d9d48fa45e09#fast" width="640" height="480"></iframe>
